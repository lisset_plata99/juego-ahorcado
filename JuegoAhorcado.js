var palabraOculta = prompt("Ingresa la palabra para empezar el juego (favor de ingresar todo junto)");
var palabraAdivinar = palabraOculta.trim().split("");
var letrasAdivinadas = []; 
var progresoUsuario = [];
var numIntentos = 5;

boton.addEventListener('click', comprobarLetraUsuario);

for (var i = 0; i < palabraAdivinar.length; i++) {
    letrasAdivinadas.push("*"); 
    resultado.innerHTML = letrasAdivinadas; 
}


function comprobarLetraUsuario() {

    var letraUsuario = document.getElementById('letra').value; 
    if (!palabraAdivinar.includes(letraUsuario)) {
        numIntentos -= 1;
        intentos.innerHTML = numIntentos;
       
        progresoUsuario.push(letraUsuario);
        historial.innerHTML = progresoUsuario;
        document.getElementById('letra').value = "";
    } else {
        for (var i = 0; i < palabraAdivinar.length; i++) {
            if (palabraAdivinar[i] == letraUsuario) { 
                letrasAdivinadas[i] = letraUsuario;
            }
        }
        resultado.innerHTML = letrasAdivinadas;
        intentos.innerHTML = numIntentos;
        document.getElementById('letra').value = "";
    }
    validarProgreso();
}

function validarProgreso() {
    if (!letrasAdivinadas.includes('*')) {
        alert('Felicidades, has ganado');
    }
    if (numIntentos == 0) {
        alert("Has perdido, suerte para la proxima, la palabra era : " + palabraOculta);
    }
}
